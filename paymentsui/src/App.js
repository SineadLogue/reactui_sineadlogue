import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import { Switch, Route, Link } from "react-router-dom";
import AddPayment from "./components/AddPayment";
import AddPaymentNoRedux from "./components/AddPaymentNoRedux";
import ListPayments from "./components/ListPayments";
import About from "./components/About";
import Welcome from "./components/Welcome";

import './App.css';
import './Payments.css'

function App() {
  return (
      <div id="container">
        <nav className="navbar navbar-expand navbar-light bg-light">
          <a href="/home" className="navbar-brand">
            Allstate
          </a>
          <div className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to={"/payments"} className="nav-link">
                Payments
              </Link>
            </li>
            <li className="nav-item">
               <Link  to={"/add"} className="nav-link">
                 Add (Redux)
               </Link>
            </li>
              <li className="nav-item">
                <Link to={"/addNoRedux"} className="nav-link">
                  Add
                </Link>
            </li>
             <li className="nav-item">
              <Link to={"/about"} className="nav-link">
                About
              </Link>
            </li>
          </div>
        </nav>
        <div className="text-center text-primary PageHeader">
          <h1 className="AppHeader">Payment Portal</h1>
        </div>

        <div className="container mt-3">
          <Switch>
            <Route exact path={["/", "/home"]} component={Welcome} />
            <Route exact path={["/", "/payments"]} component={ListPayments} />
            <Route exact path="/add" component={AddPayment} />
            <Route exact path="/addNoRedux" component={AddPaymentNoRedux} />
            <Route exact path="/about" component={About} />
          </Switch>
        </div>
      </div>
  )
}

export default App;

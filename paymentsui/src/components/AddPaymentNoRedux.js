import React, {Component, useState} from 'react';
import PaymentRestService from "../services/PaymentRestService";
import '../Payments.css'
import paymentAdd from "../redux/actions/PaymentAddActions";

export default class AddPaymentNoRedux extends Component {
    constructor (props) {
        super(props);

        this.handleInputChanges = this.handleInputChanges.bind(this);
        this.savePayment = this.savePayment.bind(this);

        this.state ={
            id: 0,
            custId: 0,
            amount: 0,
            paymentType: "",
            paymentDate:"",
            submitted:false
        };


    }

    handleInputChanges = (event) =>{
        const {name, value} = event.target;
        this.setState({[name]: value});
    };

    savePayment() {
        var data = {
            id: this.state.id,
            custId:  this.state.custId,
            amount: this.state.amount,
            paymentType: this.state.paymentType,
            paymentDate: this.state.paymentDate,
            submitted: false
        }

        PaymentRestService.create(data)
            .then(response => {
                this.setState({
                    id: response.data.id,
                    custId: response.data.custId,
                    amount: response.data.amount,
                    paymentType: this.state.paymentType,
                    paymentDate: this.state.paymentDate,
                    submitted: true
                });
            })
            .catch(e => {
                this.setState({submitted:false});
            })
    }
    render(){
        return (<form>
                <h4 className="PageHeader"> Add Payment </h4>
                <div className="form-group">
                    <label htmlFor="id">Payment ID</label>
                    <input type="number" className="form-control" id="id" name="id" aria-describedby="paymentIdHelp"
                           placeholder="Enter Payment ID"  defaultValue={this.state.id} required onChange={this.handleInputChanges}/>
                    <small id="paymentIdHelp" className="form-text text-muted">Payment Id is reference for payments.</small>
                </div>
                <div className="form-group">
                    <label htmlFor="custId">Customer ID</label>
                    <input type="number" className="form-control" id="custId" name="custId" aria-describedby="custIdHelp"
                           placeholder="Enter Customer ID" defaultValue={this.state.custId} required onChange={this.handleInputChanges} />
                    <small id="custIdHelp" className="form-text text-muted">Customer Id related to payment</small>

                </div>
                <div className="form-group">
                    <label htmlFor="paymentDate">Payment Date</label>
                    <input type="date" id="paymentDate" name="paymentDate" defaultValue={this.state.paymentDate} aria-describedby="paymentDateHelp"
                           placeholder="Enter Payment Date"   className="form-text form-control" onChange={this.handleInputChanges} />
                    <small id="paymentDateHelp" className="form-text text-muted">Date of payment</small>
                </div>
                <div className="form-group">
                    <label htmlFor="paymentTypes">Payment Type</label>
                    <input list="paymentTypes" name="paymentType" defaultValue={this.state.paymentType} id="paymentType" aria-describedby="paymentTypeHelp"
                           className="form-control" onChange={this.handleInputChanges} />
                    <small id="paymentTypeHelp" className="form-text text-muted">Type of payment</small>
                    <datalist id="paymentTypes" >
                        <option value="SEPA"/>
                        <option value="Direct Debit"/>
                        <option value="App Transfer"/>
                        <option value="In branch lodgement"></option>
                    </datalist>
                </div>

                <div>
                    <label htmlFor="amount">Amount</label>
                    <input type="text" className="form-control" id="amount" name="amount" aria-describedby="paymentAmountHelp"
                           placeholder="Enter payment amount" defaultValue={this.state.amount} onChange={this.handleInputChanges}/>
                    <small id="paymentAmountHelp" className="form-text text-muted">Amount transferred.</small>
                </div>

                <div className="form-group text-center">
                    <input type="submit" className="btn btn-sm AllstateBtn"  value="Submit" onClick={this.savePayment}/>
                </div>
            </form>

        )
    }
}
import React from 'react';
import '../FlipCard.css'

export default function FlipCard(props) {

    //console.log("<<<< in flip card :" + props.payment.paymentType + ", " + props.payment.custId + ", " + props.payment.paymentDate + ", " + props.payment.amount)
    return ( <div className="flip-card">
            <div className="flip-card-inner">
                <div className="flip-card-front">
                    <h4> Payment information</h4>
                    <hr/>
                    <h5> Payment ID : {props.payment.id}</h5>

                </div>
                <div className="flip-card-back">
                    <h4>Payment Details</h4>
                    <hr/>
                    <div className="row">
                        <div className="col-sm-6">Payment Date: {props.payment.paymentDate}</div>
                        <div className="col-sm-6">Payment Type: {props.payment.paymentType}</div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6">Payment Amount: $ {props.payment.amount}</div>
                        <div className="col-sm-6">Customer ID: {props.payment.custId}</div>
                    </div>

                </div>
            </div>
        </div>
    )

}


import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import paymentAdd from "../redux/actions/PaymentAddActions";
import '../Payments.css'

//using Redux with React hooks instead of redux connect
function AddPayment(props) {
    const dispatch = useDispatch();
    const initialFormState = {
        id: 0,
        custId: 0,
        amount: 0,
        paymentType: "",
        paymentDate:"",
        submitted:false };

    const [payment, setPayment] = useState(initialFormState);
    const [submitted, setSubmitted] = useState(false);

    const handleInputChanges = (event) =>{
        const {name, value} = event.target;
        setPayment({...payment, [name]: value});
    };

    return (<form onSubmit = {(event) => {
            event.preventDefault();
            //console.log("<<<<<< form submit <<<<<")
            dispatch(paymentAdd(payment));
            setSubmitted(true);
        }}>
            <h4 className="PageHeader"> Add Payment </h4>
            <div className="form-group">
                <label htmlFor="id">Payment ID</label>
                <input type="number" className="form-control" id="id" name="id" aria-describedby="paymentIdHelp"
                       placeholder="Enter Payment ID" value={payment.id} required onChange={handleInputChanges}/>
                <small id="paymentIdHelp" className="form-text text-muted">Payment Id is reference for payments.</small>
            </div>
            <div className="form-group">
                <label htmlFor="custId">Customer ID</label>
                <input type="number" className="form-control" id="custId" name="custId" aria-describedby="custIdHelp"
                       placeholder="Enter Customer ID" value={payment.custId} required onChange={handleInputChanges} />
                <small id="custIdHelp" className="form-text text-muted">Customer Id related to payment</small>

            </div>
            <div className="form-group">
                <label htmlFor="paymentDate">Payment Date</label>
                <input type="date" id="paymentDate" name="paymentDate" value={payment.paymentDate} aria-describedby="paymentDateHelp"
                       placeholder="Enter Payment Date"   className="form-text form-control" onChange={handleInputChanges} />
                <small id="paymentDateHelp" className="form-text text-muted">Date of payment</small>
            </div>
            <div className="form-group">
                <label htmlFor="paymentTypes">Payment Type</label>
                <input list="paymentTypes" name="paymentType" value={payment.paymentType} id="paymentType" aria-describedby="paymentTypeHelp"
                       className="form-control" onChange={handleInputChanges} />
                <small id="paymentTypeHelp" className="form-text text-muted">Type of payment</small>
                <datalist id="paymentTypes" >
                    <option value="SEPA"/>
                    <option value="Direct Debit"/>
                    <option value="App Transfer"/>
                    <option value="In branch lodgement"></option>
                </datalist>
            </div>

            <div>
                <label htmlFor="amount">Amount</label>
                <input type="text" className="form-control" id="amount" name="amount" aria-describedby="paymentAmountHelp"
                       placeholder="Enter payment amount" value={payment.amount} onChange={handleInputChanges}/>
                <small id="paymentAmountHelp" className="form-text text-muted">Amount transferred.</small>
            </div>

            <div className="form-group text-center">
                <input type="submit" className="btn btn-sm AllstateBtn"  value="Submit"/>
            </div>
            { ({submitted} !== undefined && submitted) ? (
                <label className="text-success">Payment Added Successfully</label>
            ) : (
                <label className="text-warning">Payment Not Added Yet</label>
            )}
        </form>

)
}
export default AddPayment;

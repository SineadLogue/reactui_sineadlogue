import React, {Component} from 'react'

export default class About extends Component{
    constructor() {
        super();
        this.state = {name: "Allstate", year: "2020", apistatus: "Unknown"};
    }
    render(){
        return (<div className="container">
                  <h4 className="PageHeader"> Status Information </h4>
                    <div className="InformationPanel">
                         <h4>Name is {this.state.name}</h4>
                         <h4>Year is {this.state.year}</h4>
                         {this.state.apistatus !== "Unknown" ? (
                           <label className="text-info">Status is {this.state.apistatus}</label>
                            ) : (
                               <label className="text-warning">Statis is unknowm</label>
                            )
                         }
                    </div>
                 </div>)
    }

    async componentDidMount() {
        var url = "http://localhost:8080/payments/status";
        var testData;
        try {
            let response = await fetch(url);
            testData = await response.text();
            console.log('the data is ' + testData);
        }
        catch (e) {
            console.log("the error is " + e);

        }
        this.setState({apistatus: testData});
    }

}

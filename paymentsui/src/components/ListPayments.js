import React, {Component} from 'react';
import FlipCard from './FlipCard'
import paymentList from "../redux/actions/PaymentListActions";
import {connect} from 'react-redux';
import '../Payments.css'

class  ListPayments extends  Component {
    constructor(props) {
        super(props);
        this.props.dispatch(paymentList())
    }

    render() {
        //console.log("<<<< rendering payment list");
        const {payments, pending, error} = this.props;
        //console.log("rendering payments: " + payments);
        return (
            <div className="text-center">
                <h4 className="PageHeader">Payments List </h4>

                {payments &&
                payments.map((payment, index) => (
                    <FlipCard payment={payment} key={index} />
                ))}
            </div>

        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: () => dispatch(paymentList())
    };
};
const mapStateToProps = state => ({
    error:state.error,
    payments: state.payments,
    pending: state.pending
})


export default connect(mapStateToProps,mapDispatchToProps)(ListPayments);


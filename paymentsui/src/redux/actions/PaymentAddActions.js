
import {PAYMENT_ADD_REQUEST , PAYMENT_ADD_SUCCESS , PAYMENT_ADD_FAIL}
    from "../constants/PaymentConstants"


import PaymentRestService from "../../services/PaymentRestService";

const paymentAdd = (payment) => async  (dispatch) => {
    //console.log("<<<<<<<Payment Add action <<<<<");
    try {
        dispatch({type: PAYMENT_ADD_REQUEST, payload: payment});
        const newPayment = await PaymentRestService.create(payment);
        dispatch({type: PAYMENT_ADD_SUCCESS, payload: newPayment});
    } catch (error) {
        dispatch({type: PAYMENT_ADD_FAIL, payload: error});
    }
};



export default  paymentAdd;
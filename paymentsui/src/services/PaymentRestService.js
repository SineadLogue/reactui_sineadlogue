import http from '../httpCommon';

export class PaymentRestService {
    getAll() {
        return http.get("/list");
    }

    get(id){
        return http.get(`/find/${id}`);
    }

    create(data){
        return http.post("/add", data);
    }
}

export default new PaymentRestService();   // intance of class so when import it will have an instance of it

